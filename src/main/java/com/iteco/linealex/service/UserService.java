package com.iteco.linealex.service;

import com.iteco.linealex.api.repository.IUserRepository;
import com.iteco.linealex.api.service.IUserService;
import com.iteco.linealex.enumerate.Role;
import com.iteco.linealex.model.User;
import com.iteco.linealex.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;

@Transactional
@Service("userService")
public final class UserService extends AbstractService<User> implements IUserService {

    @Value("${PASSWORD_SALT}")
    private String passwordSalt;

    @Value("${PASSWORD_TIMES}")
    private String passwordTimes;

    @Value("${ADMIN}")
    private String adminLogin;

    @Value("${ADMIN_PASSWORD}")
    private String adminPassword;

    @Value("${USER}")
    private String userLogin;

    @Value("${USER_PASSWORD}")
    private String userPassword;

    @Autowired
    private IUserRepository userRepository;

    @PostConstruct
    void init() throws NoSuchAlgorithmException {
        userRepository.deleteAll();
        @NotNull final User admin = new User();
        admin.setLogin(adminLogin);
        admin.setHashPassword(TransformatorToHashMD5.getHash(adminPassword));
        admin.setRole(Role.ADMINISTRATOR);
        userRepository.save(admin);
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setHashPassword(TransformatorToHashMD5.getHash(userPassword));
        userRepository.save(user);
    }

    @Nullable
    @Override
    public User getEntityById(@Nullable final String entityId) {
        if (entityId == null) return null;
        return userRepository.findById(entityId).get();
    }

    @NotNull
    @Override
    public Collection<User> getAllEntities() {
        return (Collection<User>) userRepository.findAll();
    }

    @Nullable
    @Override
    public User logInUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception("Incorrect login");
        if (password == null || password.isEmpty()) throw new Exception("Wrong password");
        return userRepository.findOneByLogin(login);
    }

    @Override
    @Transactional
    public void createUser(
            @Nullable final User user,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) return;
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new Exception("Low access level");
        if (user == null) return;
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void persist(@Nullable final User entity) {
        if (entity == null) return;
        if (entity.getLogin().isEmpty()) return;
        if (entity.getHashPassword() == null || entity.getHashPassword().isEmpty()) return;
        userRepository.save(entity);
    }

    @Override
    public void persist(@NotNull Collection<User> collection) {
        if (collection.isEmpty()) return;
        for (@Nullable final User user : collection) {
            if (user == null) continue;
            persist(user);
        }
    }

    @Override
    @Transactional
    public void removeEntity(@Nullable final String entityId) {
        if (entityId == null) return;
        userRepository.deleteById(entityId);
    }

    @Override
    @Transactional
    public void removeAllEntities() {
        userRepository.deleteAll();
    }

    @Override
    @Transactional
    public void merge(@Nullable final User entity) {
        if (entity == null) return;
        userRepository.save(entity);
    }

    @Transactional
    public void updateUserPassword(
            @Nullable final String oldPassword,
            @Nullable final String newPassword,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) throw new Exception("User is not log in");
        if (oldPassword == null || oldPassword.isEmpty()) throw new Exception("Wrong password");
        @NotNull final String hashOldPassword = TransformatorToHashMD5.getHash(oldPassword);
        if (selectedUser.getHashPassword() == null
                && !selectedUser.getHashPassword().equals(hashOldPassword)) throw new Exception("Wrong password");
        if (newPassword == null || newPassword.isEmpty()) throw new Exception("Wrong password");
        if (newPassword.length() < 8) throw new Exception("Short password");
        @NotNull final String hashNewPassword = TransformatorToHashMD5.getHash(newPassword);
        selectedUser.setHashPassword(hashNewPassword);
        userRepository.save(selectedUser);
    }

    @Nullable
    public User getUser(@Nullable final String login) {
        return userRepository.findOneByLogin(login);
    }

}