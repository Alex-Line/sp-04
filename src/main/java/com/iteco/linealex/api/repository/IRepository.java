package com.iteco.linealex.api.repository;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public interface IRepository<T> {

    boolean contains(@NotNull final String entityId);

    @NotNull
    Collection<T> findAll();

    void persist(@NotNull final T example);

    void persist(@NotNull final Collection<T> collection);

    void merge(@NotNull final T example);

    void remove(@NotNull final String name);

    void removeAll();

}