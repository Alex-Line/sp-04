package com.iteco.linealex.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IService<T> {

    void persist(@Nullable final T entity);

    void persist(@NotNull final Collection<T> collection);

    void merge(@Nullable final T entity);

    @Nullable
    T getEntityById(@Nullable final String entityId);

    @NotNull
    Collection<T> getAllEntities();

    void removeEntity(@Nullable final String entityId);

    void removeAllEntities();

}