package com.iteco.linealex.rest_controller;

import com.iteco.linealex.api.service.IUserService;
import com.iteco.linealex.dto.UserDto;
import com.iteco.linealex.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;

@RestController
@RequestMapping("/api/user")
public class UserRestController {

    @Autowired
    IUserService userService;

    //C in CRUD
    @PostMapping(value = "create", produces = MediaType.APPLICATION_JSON_VALUE)
    public void create(
            @RequestBody final UserDto userDto
    ) throws Exception {
        @NotNull final User user = UserDto.toUser(userDto);
        userService.persist(user);
    }

    //R in CRUD
    @GetMapping(value = {"list"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<UserDto> getAllUsers(){
        return User.toUsersDto(new ArrayList<User>(userService.getAllEntities()));
    }

    //R in CRUD
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDto getUserById(@PathVariable final String id){
        return User.toUserDto(userService.getEntityById(id));
    }

    //U in CRUD
    @PutMapping(value = "update", produces = MediaType.APPLICATION_JSON_VALUE)
    public void update(
            @RequestBody final UserDto userDto
    ) throws Exception {
        @Nullable User user = userService.getEntityById(userDto.getId());
        if(user != null) user = UserDto.toUser(userDto);
        userService.merge(user);
    }

    //D in CRUD
    @DeleteMapping(value = "delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable final String id) {
        userService.removeEntity(id);
    }

}