package com.iteco.linealex.client.feign;

import com.iteco.linealex.dto.UserDto;
import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@FeignClient("api/user")
public interface UserClient {

    static UserClient client(@NotNull final String baseUrl){
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(UserClient.class, baseUrl);
    }

    @PostMapping(value = "create", produces = MediaType.APPLICATION_JSON_VALUE)
    void create(@RequestBody final UserDto userDto) throws Exception;

    @GetMapping(value = {"/","list"}, produces = MediaType.APPLICATION_JSON_VALUE)
    Collection<UserDto> getAllUsers();

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    UserDto getUserById(@PathVariable final String id);

    @PutMapping(value = "update", produces = MediaType.APPLICATION_JSON_VALUE)
    void update(@PathVariable final UserDto userDto) throws Exception;

    @DeleteMapping(value = "delete", produces = MediaType.APPLICATION_JSON_VALUE)
    void delete(@PathVariable final String id);

}
