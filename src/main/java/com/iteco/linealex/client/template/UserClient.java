package com.iteco.linealex.client.template;

import com.iteco.linealex.dto.UserDto;
import com.iteco.linealex.model.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;

public final class UserClient {

    public static void create(
            @NotNull final User user
    ) {
        @NotNull final String url = "http://localhost:8080/api/user/create";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<UserDto> entity = new HttpEntity<>(User.toUserDto(user), headers);
        template.postForObject(url, entity, Object.class);
    }

    public static Collection<UserDto> getAllUsers(){
        @NotNull final String url = "http://localhost:8080/api/user/list";
        @NotNull final RestTemplate template = new RestTemplate();
        return (Collection<UserDto>) template.getForObject(url, Collection.class);
    }

    public static UserDto getUserById(@NotNull final String id){
        @NotNull final String url = "http://localhost:8080/api/user/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(url, UserDto.class, id);
    }

    public static void update(@NotNull final User user) {
        @NotNull final String url = "http://localhost:8080/api/user/update";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<UserDto> entity = new HttpEntity<>(User.toUserDto(user), headers);
        template.put(url, entity);
    }

    public static void delete(@NotNull final String id) {
        @NotNull final String url = "http://localhost:8080/api/user/delete/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        template.delete(url, id);
    }

}