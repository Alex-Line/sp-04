package com.iteco.linealex.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.iteco.linealex.api.service.IProjectService;
import com.iteco.linealex.api.service.ITaskService;
import com.iteco.linealex.enumerate.Role;
import com.iteco.linealex.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.UUID;

@Getter
@Setter
@Component
@NoArgsConstructor
public class UserDto {

    @Nullable
    static IProjectService staticProjectService;

    @Nullable
    static ITaskService staticTaskService;

    @Autowired
    @JsonIgnore
    IProjectService projectService;

    @Autowired
    @JsonIgnore
    ITaskService taskService;

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String login = "unnamed";

    @Nullable
    private String hashPassword;

    @NotNull
    private Role role = Role.ORDINARY_USER;

    @PostConstruct
    void init(){
        staticProjectService = projectService;
        staticTaskService = taskService;
    }

    @NotNull
    public static User toUser(@NotNull final UserDto userDto) throws Exception {
        @NotNull final User user = new User();
        user.setId(userDto.getId());
        user.setLogin(userDto.getLogin());
        user.setHashPassword(userDto.getHashPassword());
        user.setRole(userDto.getRole());
        user.setProjects(new ArrayList<>(staticProjectService.getAllEntities(userDto.getId())));
        user.setTasks(new ArrayList<>(staticTaskService.getAllEntities(userDto.getId())));
        return user;
    }

}