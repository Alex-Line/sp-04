import com.iteco.linealex.client.template.UserClient;
import com.iteco.linealex.enumerate.Role;
import com.iteco.linealex.model.User;
import com.iteco.linealex.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class UserIntegrationTest {

    @NotNull final User test = new User();

    @Test
    public void listUsersIntegrationTestPositive(){
        System.out.println(UserClient.getAllUsers());
        Assert.assertNotNull(UserClient.getAllUsers());
        Assert.assertTrue(UserClient.getAllUsers().toString().contains("admin"));
    }

    @Test
    public void getUserIntegrationTestPositive() throws Exception {
        test.setLogin("test");
        test.setRole(Role.ORDINARY_USER);
        test.setHashPassword(TransformatorToHashMD5.getHash("11111111"));
        test.setTasks(new ArrayList<>());
        test.setProjects(new ArrayList<>());
        UserClient.create(test);
        Assert.assertNotNull(UserClient.getUserById(test.getId()));
        Assert.assertTrue(UserClient.getUserById(test.getId()).getLogin().contains("test"));
        UserClient.delete(test.getId());
    }

    @Test
    public void getUserIntegrationTestNegative() throws Exception {
        test.setLogin("test");
        test.setRole(Role.ORDINARY_USER);
        test.setHashPassword(TransformatorToHashMD5.getHash("11111111"));
        test.setTasks(new ArrayList<>());
        test.setProjects(new ArrayList<>());
        UserClient.create(test);
        Throwable thrown = assertThrows(Exception.class, () -> {
            UserClient.getUserById(UUID.randomUUID().toString());
        });
        Assert.assertNotNull(thrown.getMessage());
        UserClient.delete(test.getId());
    }

    @Test
    public void createUserIntegrationTestPositive() throws Exception {
        test.setLogin("test");
        test.setRole(Role.ORDINARY_USER);
        test.setHashPassword(TransformatorToHashMD5.getHash("11111111"));
        test.setTasks(new ArrayList<>());
        test.setProjects(new ArrayList<>());
        UserClient.create(test);
        Assert.assertNotNull(UserClient.getUserById(test.getId()));
        Assert.assertTrue(UserClient.getUserById(test.getId()).getLogin().contains("test"));
        UserClient.delete(test.getId());
    }

    @Test
    public void updateUserIntegrationTestPositive() throws Exception {
        test.setLogin("test");
        test.setRole(Role.ORDINARY_USER);
        test.setHashPassword(TransformatorToHashMD5.getHash("11111111"));
        test.setTasks(new ArrayList<>());
        test.setProjects(new ArrayList<>());
        UserClient.create(test);
        Assert.assertNotNull(UserClient.getUserById(test.getId()));
        Assert.assertTrue(UserClient.getUserById(test.getId()).getLogin().contains("test"));
        test.setRole(Role.ADMINISTRATOR);
        UserClient.update(test);
        Assert.assertNotNull(UserClient.getUserById(test.getId()));
        Assert.assertSame(UserClient.getUserById(test.getId()).getRole(), Role.ADMINISTRATOR);
        UserClient.delete(test.getId());
    }

    @Test
    public void updateUserIntegrationTestNegative() throws Exception {
        test.setLogin("test");
        test.setRole(Role.ORDINARY_USER);
        test.setHashPassword(TransformatorToHashMD5.getHash("11111111"));
        test.setTasks(new ArrayList<>());
        test.setProjects(new ArrayList<>());
        UserClient.create(test);
        Assert.assertNotNull(UserClient.getUserById(test.getId()));
        Assert.assertTrue(UserClient.getUserById(test.getId()).getLogin().contains("test"));
        test.setRole(Role.ADMINISTRATOR);
        UserClient.update(test);
        Assert.assertNotSame(UserClient.getUserById(test.getId()).getRole(), Role.ORDINARY_USER);
        UserClient.delete(test.getId());
    }

    @Test
    public void deleteUserIntegrationTestPositive() throws Exception {
        test.setLogin("test");
        test.setRole(Role.ORDINARY_USER);
        test.setHashPassword(TransformatorToHashMD5.getHash("11111111"));
        test.setTasks(new ArrayList<>());
        test.setProjects(new ArrayList<>());
        UserClient.create(test);
        Assert.assertNotNull(UserClient.getUserById(test.getId()));
        Assert.assertTrue(UserClient.getUserById(test.getId()).getLogin().contains("test"));
        UserClient.delete(test.getId());
        Throwable thrown = assertThrows(Exception.class, () -> {
            UserClient.getUserById(test.getId());
        });
        Assert.assertNotNull(thrown.getMessage());
    }

}