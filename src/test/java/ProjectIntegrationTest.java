import com.iteco.linealex.client.template.ProjectClient;
import com.iteco.linealex.client.template.UserClient;
import com.iteco.linealex.enumerate.Role;
import com.iteco.linealex.enumerate.Status;
import com.iteco.linealex.model.Project;
import com.iteco.linealex.model.User;
import com.iteco.linealex.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ProjectIntegrationTest {

    @NotNull final User user = new User();

    @NotNull final Project test = new Project();

    @Test
    public void listUsersIntegrationTestPositive() throws NoSuchAlgorithmException {
        user.setLogin("user");
        user.setRole(Role.ORDINARY_USER);
        user.setHashPassword(TransformatorToHashMD5.getHash("11111111"));
        user.setTasks(new ArrayList<>());
        user.setProjects(new ArrayList<>());
        UserClient.create(user);
        test.setName("test");
        test.setDescription("test description");
        test.setUser(user);
        test.setStatus(Status.PLANNED);
        ProjectClient.create(test);
        Assert.assertNotNull(ProjectClient.getAllProjects());
        Assert.assertTrue(ProjectClient.getAllProjects().toString().contains("test"));
        ProjectClient.delete(test.getId());
        UserClient.delete(user.getId());
    }

    @Test
    public void getUserIntegrationTestPositive() throws Exception {
        user.setLogin("user");
        user.setRole(Role.ORDINARY_USER);
        user.setHashPassword(TransformatorToHashMD5.getHash("11111111"));
        user.setTasks(new ArrayList<>());
        user.setProjects(new ArrayList<>());
        UserClient.create(user);
        test.setName("test");
        test.setDescription("test description");
        test.setUser(user);
        test.setStatus(Status.PLANNED);
        ProjectClient.create(test);
        Assert.assertNotNull(ProjectClient.getProjectById(test.getId()).getId());
        Assert.assertTrue(ProjectClient.getProjectById(test.getId()).getName().contains("test"));
        ProjectClient.delete(test.getId());
        UserClient.delete(user.getId());
    }

    @Test
    public void getUserIntegrationTestNegative() throws Exception {
        user.setLogin("user");
        user.setRole(Role.ORDINARY_USER);
        user.setHashPassword(TransformatorToHashMD5.getHash("11111111"));
        user.setTasks(new ArrayList<>());
        user.setProjects(new ArrayList<>());
        UserClient.create(user);
        test.setName("test");
        test.setDescription("test description");
        test.setUser(user);
        test.setStatus(Status.PLANNED);
        ProjectClient.create(test);
        Throwable thrown = assertThrows(Exception.class, () -> {
            ProjectClient.getProjectById(UUID.randomUUID().toString());
        });
        Assert.assertNotNull(thrown.getMessage());
        ProjectClient.delete(test.getId());
        UserClient.delete(user.getId());
    }

    @Test
    public void createUserIntegrationTestPositive() throws Exception {
        user.setLogin("user");
        user.setRole(Role.ORDINARY_USER);
        user.setHashPassword(TransformatorToHashMD5.getHash("11111111"));
        user.setTasks(new ArrayList<>());
        user.setProjects(new ArrayList<>());
        UserClient.create(user);
        test.setName("test");
        test.setDescription("test description");
        test.setUser(user);
        test.setStatus(Status.PLANNED);
        ProjectClient.create(test);
        Assert.assertNotNull(ProjectClient.getProjectById(test.getId()).getId());
        Assert.assertTrue(ProjectClient.getProjectById(test.getId()).getName().contains("test"));
        ProjectClient.delete(test.getId());
        UserClient.delete(user.getId());
    }

    @Test
    public void updateUserIntegrationTestPositive() throws Exception {
        user.setLogin("user");
        user.setRole(Role.ORDINARY_USER);
        user.setHashPassword(TransformatorToHashMD5.getHash("11111111"));
        user.setTasks(new ArrayList<>());
        user.setProjects(new ArrayList<>());
        UserClient.create(user);
        test.setName("test");
        test.setDescription("test description");
        test.setUser(user);
        test.setStatus(Status.PLANNED);
        ProjectClient.create(test);
        Assert.assertNotNull(ProjectClient.getProjectById(test.getId()).getId());
        Assert.assertTrue(ProjectClient.getProjectById(test.getId()).getName().contains("test"));
        test.setStatus(Status.DONE);
        ProjectClient.update(test);
        Assert.assertNotNull(ProjectClient.getProjectById(test.getId()).getId());
        Assert.assertSame(ProjectClient.getProjectById(test.getId()).getStatus(), Status.DONE);
        ProjectClient.delete(test.getId());
        UserClient.delete(user.getId());
    }

    @Test
    public void updateUserIntegrationTestNegative() throws Exception {
        user.setLogin("user");
        user.setRole(Role.ORDINARY_USER);
        user.setHashPassword(TransformatorToHashMD5.getHash("11111111"));
        user.setTasks(new ArrayList<>());
        user.setProjects(new ArrayList<>());
        UserClient.create(user);
        test.setName("test");
        test.setDescription("test description");
        test.setUser(user);
        test.setStatus(Status.PLANNED);
        ProjectClient.create(test);
        Assert.assertNotNull(ProjectClient.getProjectById(test.getId()).getId());
        Assert.assertTrue(ProjectClient.getProjectById(test.getId()).getName().contains("test"));
        test.setStatus(Status.DONE);
        ProjectClient.update(test);
        Assert.assertNotSame(ProjectClient.getProjectById(test.getId()).getStatus(), Status.PROCESSING);
        ProjectClient.delete(test.getId());
        UserClient.delete(user.getId());
    }

    @Test
    public void deleteUserIntegrationTestPositive() throws Exception {
        user.setLogin("user");
        user.setRole(Role.ORDINARY_USER);
        user.setHashPassword(TransformatorToHashMD5.getHash("11111111"));
        user.setTasks(new ArrayList<>());
        user.setProjects(new ArrayList<>());
        UserClient.create(user);
        test.setName("test");
        test.setDescription("test description");
        test.setUser(user);
        test.setStatus(Status.PLANNED);
        ProjectClient.create(test);
        Assert.assertNotNull(ProjectClient.getProjectById(test.getId()).getId());
        Assert.assertTrue(ProjectClient.getProjectById(test.getId()).getName().contains("test"));
        ProjectClient.delete(test.getId());
        UserClient.delete(user.getId());
        Throwable thrown = assertThrows(Exception.class, () -> {
            ProjectClient.getProjectById(test.getId()).getId();
        });
        Assert.assertNotNull(thrown.getMessage());
    }

}